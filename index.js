function QuoteBox() {
  const [quote, setQuote] = React.useState("");
  const [author, setAuthor] = React.useState("");
  const [color, setColor] = React.useState("#fff");

  const fetchNewQuote = async () => {
    const response = await fetch("https://type.fit/api/quotes");
    const data = await response.json();

    const randomIndex = Math.floor(Math.random() * data.length);
    const randomQuote = data[randomIndex];
    setQuote(randomQuote.text);
    setAuthor(randomQuote.author || "Unknown");
    setColor(getRandomColor());
  };

  const getRandomColor = () => {
    const colors = [
      "#16a085",
      "#27ae60",
      "#2c3e50",
      "#f39c12",
      "#e74c3c",
      "#9b59b6",
      "#FB6964",
      "#342224",
      "#472E32",
      "#BDBB99",
      "#77B1A9",
      "#73A857",
    ];
    return colors[Math.floor(Math.random() * colors.length)];
  };

  React.useEffect(() => {
    fetchNewQuote();
  }, []);

  const tweetUrl = `https://twitter.com/intent/tweet?text=${encodeURIComponent(
    `"${quote}" - ${author}`
  )}`;
  const buttonStyles = {
    backgroundColor: color,
    borderColor: color,
  };

  const twitterIconStyle = {
    color: "white",
  };

  return (
    <div
      id="quote-box"
      style={{
        backgroundColor: color,
        minHeight: "100vh",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        // position: "relative",
      }}
    >
      <div className="container" style={{ maxWidth: "600px" }}>
        <div className="jumbotron">
          <div className="card">
            <div className="card-header">Inspirational Quotes</div>
            <div
              className="card-body"
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <p
                id="text"
                style={{
                  fontSize: "24px",
                  fontWeight: "bold",
                  textAlign: "center",
                  lineHeight: "1.4",
                }}
              >
                &quot;{quote}
              </p>
              <div
                className="author-container"
                style={{ alignSelf: "flex-end" }}
              >
                <p id="author" style={{ textAlign: "right" }}>
                  - {author}
                </p>
              </div>

              <div
                className="button-container"
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "flex-end",
                  position: "relative",
                  bottom: "10px",
                  width: "100%",
                }}
              >
                <div style={{ display: "flex" }}>
                  <a
                    id="tweet-quote"
                    href={tweetUrl}
                    target="_blank"
                    rel="noopener noreferrer"
                    className="btn btn-warning"
                    style={{ flex: "1", marginRight: "5px", ...buttonStyles }}
                  >
                    <i className="fa fa-twitter" style={twitterIconStyle}></i>
                  </a>
                  <a
                    href={"https://www.tumblr.com/"}
                    target="_blank"
                    className="btn btn-danger"
                    style={{ flex: "1", ...buttonStyles }}
                  >
                    <i className="fa fa-tumblr"></i>
                  </a>
                </div>
                <button
                  id="new-quote"
                  onClick={fetchNewQuote}
                  className="btn btn-primary ml-3"
                  style={{ ...buttonStyles }}
                >
                  New quote
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

function App() {
  return (
    <div>
      <QuoteBox />
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById("app"));
